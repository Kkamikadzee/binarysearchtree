﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree
{
    public class BinarySearchTreeNode
    {
        private string _data;
        public BinarySearchTreeNode _left;
        public BinarySearchTreeNode _right;

        public BinarySearchTreeNode RightNode { get { return _right; } set { _right = value; } }
        public BinarySearchTreeNode LeftNode { get { return _left; } set { _left = value; } }
        public string Data { get { return _data; } }

        public BinarySearchTreeNode() { }
        public BinarySearchTreeNode(BinarySearchTreeNode tmp)
        {
            _data = tmp._data;
            _left = tmp._left;
            _right = tmp._right;
        }
        public BinarySearchTreeNode(string data)
        {
            _data = data;
        }
    }
}
