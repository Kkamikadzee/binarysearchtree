﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree
{
    class BinarySearchTree
    {
        private BinarySearchTreeNode _root;
        public BinarySearchTreeNode Root { get { return _root; } }

        public BinarySearchTree() { }
        public BinarySearchTree(string data)
        {
            Insert(data);
        }

        public void Clear()
        {
            _cleareNode(_root);
        }
        private void _cleareNode(BinarySearchTreeNode pV)
        {
            if(pV != null)
            {
                if (pV.LeftNode != null)
                    _cleareNode(pV.LeftNode);
                if (pV.RightNode != null)
                    _cleareNode(pV.RightNode);
                pV = null;
                System.GC.Collect();
            }
        }

        private int StringСomparison(string str1, string str2)
        {
            if(str1 == null)
                return 2;
            for (int i = 0; i < str1.Length && i < str2.Length; i++)
            {
                if (str1.Length == i && str2.Length != i)
                    return 1;
                else if (str1.Length != i && str2.Length == i)
                    return 2;
                else if (str1[i] == str2[i])
                    continue;
                else
                {
                    if (str1[i] > str2[i])
                    {
                        return 1;
                    }
                    else
                    {
                        return 2;
                    }
                }
            }
            return 0;
        }

        public void Insert(string data)
        {
            _insertNode(ref _root, data);
        }
        private void _insertNode(ref BinarySearchTreeNode pV, string data)
        {
            if (pV == null)
            {
                pV = new BinarySearchTreeNode(data);
            }
            else
            {
                int tmp = StringСomparison(pV.Data, data);
                switch (tmp)
                {
                    case 0:
                        return;
                    case 1:
                        _insertNode(ref pV._left, data);
                        break;
                    case 2:
                        _insertNode(ref pV._right, data);
                        break;
                }
            }
        }

        public void Remove(string data)
        {
            _removeNode(ref _root, data);
        }
        private void _removeNode(ref BinarySearchTreeNode pV, string data)
        {
            if (StringСomparison(pV.Data, data) == 0)
            {
                BinarySearchTreeNode delNode = new BinarySearchTreeNode(pV);
                if (pV.LeftNode == null && pV.RightNode == null)
                {
                    pV = null;
                    System.GC.Collect();
                }
                else
                {
                    if (pV.LeftNode == null)
                        pV = pV.RightNode;
                    else
                    {
                        if (pV.RightNode == null)
                            pV = pV.LeftNode;
                        else
                        {
                            BinarySearchTreeNode p = new BinarySearchTreeNode(pV);
                            if (pV.LeftNode != null)
                            {
                                BinarySearchTreeNode i = new BinarySearchTreeNode(pV.LeftNode);
                                while (i.RightNode != null)
                                {
                                    p = new BinarySearchTreeNode(i);
                                    i = new BinarySearchTreeNode(i.RightNode);
                                }
                                pV = new BinarySearchTreeNode(i);
                                p.RightNode = new BinarySearchTreeNode(i.LeftNode);
                                i.RightNode = new BinarySearchTreeNode(delNode.RightNode);
                                i.LeftNode = new BinarySearchTreeNode(p);
                            }
                            else
                            {
                                BinarySearchTreeNode i = new BinarySearchTreeNode(pV.RightNode);
                                while (i.LeftNode != null)
                                {
                                    p = new BinarySearchTreeNode(i);
                                    i = new BinarySearchTreeNode(i.LeftNode);
                                }
                                pV = new BinarySearchTreeNode(i);
                                p.LeftNode = new BinarySearchTreeNode(i.LeftNode);
                                i.LeftNode = new BinarySearchTreeNode(delNode.LeftNode);
                                i.RightNode = new BinarySearchTreeNode(p);
                            }
                        }
                    }
                }
            }
            else
            {
                if (StringСomparison(pV.Data, data) == 1)
                    _removeNode(ref pV._left, data);
                else
                    _removeNode(ref pV._right, data);
            }
        }

        public BinarySearchTreeNode Search(string data)
        {
            return _searchNode(_root, data);
        }
        private BinarySearchTreeNode _searchNode(BinarySearchTreeNode pV, string data)
        {
            if (StringСomparison(pV.Data, data) == 0)
                return pV;
            else if (StringСomparison(pV.Data, data) == 1 || StringСomparison(pV.Data, data) == 2)
            {
                if (StringСomparison(pV.Data, data) == 1)
                {
                    _searchNode(pV.LeftNode, data);
                    return null;
                }
                else
                {
                    _searchNode(pV.RightNode, data);
                    return null;
                }
            }
            else
                return null;
        }

        public string Write()
        {
            return _writeNode(_root, 0);
        }
        private string _writeNode(BinarySearchTreeNode pV, int space)
        {
            string returnStr = "";
            while(pV!=null)
            {
                if (pV.RightNode != null)
                    returnStr += _writeNode(pV.RightNode, space + 5);
                string strSpace = "";
                for (int i = 0; i < space; i++)
                    strSpace += " ";
                returnStr += strSpace + pV.Data + "\n";
                if (pV.LeftNode != null)
                    pV = new BinarySearchTreeNode(pV.LeftNode);
                else
                    pV = null;
                space += 5;
            }
            return returnStr;
        }
    }
}
