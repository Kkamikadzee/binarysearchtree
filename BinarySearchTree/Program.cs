﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree
{
    class Program
    {
        static void Main(string[] args)
        {
            BinarySearchTree bst = new BinarySearchTree("c");
            bst.Insert("b");
            bst.Insert("d");
            bst.Insert("a");
            bst.Insert("e");
            Console.WriteLine(bst.Write());
            bst.Remove("d");
            Console.WriteLine(bst.Write());
            bst.Clear();
            Console.WriteLine(bst.Write());
            Console.ReadKey();
        }
    }
}
